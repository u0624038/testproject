import React, { Component } from 'react'
import { render } from 'react-dom'
import { HashRouter, Route, Link } from 'react-router-dom'
import {background_rec} from './config'
import {end} from './config'
import './app.css'

var list_q=[];
var list_t=[];
var list_i=[];
var list_v=[];
var list_c=[];
var list_o=[];
var total_t=[];
var count =0;
var len_q =0;
var len_o =0;
var vid = 0;

class Time extends React.Component {
  
  constructor(props) {
    super(props);
    //console.log(props.location.state.interviewid);
    
    this.state = { seconds: 120 ,interviewid:props.location.state.interviewid,timelose:0,seconds_think:10,audioid:props.location.state.audioid,videoid:props.location.state.videoid};
    
    this.handle_data();
    
  }

  handle_data(){
    /*var URL = require('url');
    var ur = document.location.href;
    var p = URL.parse(testUrl, true).query;
    if(p.id == undefined){
      var id = this.state.value;
    }else{
      var id = p.id;
    }
    console.log(ur);*/
    var URL = require('url');
    var ur = document.location.href;
    var p = URL.parse(ur, true).query;
    if(p.id == undefined){
      var id = this.state.interviewid;
      vid = this.state.interviewid;
    }else{
      var id = p.id;
      vid = p.id;
      console.log(vid);
    }

    
    var data = {"InterviewID":id};
    var json = JSON.stringify(data);
    //console.log(json);
    var request = new XMLHttpRequest();
    request.open('POST', '/request_question');
    request.setRequestHeader('content-type' , 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState == 4) {
          
            var result =JSON.parse(request.response);
            len_q = result['QuestionList'].length;
            //console.log(result['QuestionList'])
            total_t.push(result['TotalTime']);
            for (var i=0;i<len_q;i++)
            {
              list_q.push(result['QuestionList'][i]['Content']);
              list_t.push(result['QuestionList'][i]['Time']);
              list_i.push(result['QuestionList'][i]['ItemID']);
              list_v.push(result['QuestionList'][i]['VoicePath']);
              list_c.push("Null");
              list_o.push("Null");
            }
            //c=list_t[0];
            
            //console.log(list_q);
            //console.log(list_t);
            //document.getElementById("total_time").textContent ='計時' + list_t[0] + '秒';
            
            document.getElementById("questions").append(list_q[0]) ;
            document.getElementById('voice').src = list_v[0];
            document.getElementById("voice").autoplay;
            document.getElementById("voice").controls = false;
            document.getElementById("img1").src = './img/1000x337.gif';
            console.log(list_v[0]);

            document.getElementById("area").style.width = "200px";
            document.getElementById("area").style.height = "30px";
            document.getElementById("myDIV").style.display = "none";
            
            //console.log(result);
            
            
            
        }
    }
    
    
    request.send(json)

  }
  
  componentDidMount() {
  
    let self = this

    var iframe = document.getElementById("ifrmid")

    iframe.onload = function(){
      iframe.contentWindow.postMessage(
            self.state.audioid,
            background_rec
            );
            iframe.contentWindow.postMessage(
              self.state.videoid,
              background_rec
              );
              iframe.contentWindow.postMessage(
                3,
                background_rec
                );
    }        
    if (document.getElementById("questions").textContent!=null)
      {
        this.interval = setInterval(() =>this.Timer_think(), 1000);
        
        //console.log(this.state.seconds)
      }
      
    document.oncontextmenu=function(e){return false;} 
    var selectmenu=document.getElementById("mymenu");
    selectmenu.onchange=function(){ //run some code when "onchange" event fires
      var chosenoption=this.options[this.selectedIndex].value; //this refers to "selectmenu"
      var chagefz=document.getElementById("div_changesize");
      chagefz.style.fontSize=Number(chosenoption)+ "px" ;
    }

    var text = document.getElementById("speach"); // 取得最終的辨識訊息控制項 textBox
    var textend = document.getElementById("speachend");
    var recognition = new webkitSpeechRecognition();
    // set params
    recognition.continuous = false;
    
    recognition.interimResults = true;

    // start immediately
    // recognition.start();
    
    recognition.onresult = function(event) {
      var result = event.results[event.results.length - 1];
      text.innerHTML = result[result.length - 1 ].transcript;
      
    }

    // speech error handling
    recognition.onerror = function(event){
      //console.log('error', event);
    }

    recognition.onend = function() {
      //console.log("end");
      // auto restart
      textend.innerHTML += text.innerHTML;
      text.innerHTML = '';
      recognition.start();
    }
    
    recognition.start();
  }

  componentWillUnmount() {
    
    
  }

  nextquestion(){
    var textend = document.getElementById("speachend");
    //console.log(textend.innerText);
    //console.log(list_i[count]);
    

    var data = {"ItemID":list_i[count],"Words":textend.innerText};
    
    var json = JSON.stringify(data);
    //console.log(json);
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/FollowQuestion');
    xhr.setRequestHeader('content-type' , 'application/json');
    xhr.onload = function () {
      //console.log(xhr.response);
      var a =JSON.parse(xhr.response);
      //console.log(a['ItemID']);
      //console.log(a['QID']);
      if (a['ItemID'] == 'None'){
        //console.log('No');
      }else{
        //console.log('Yes');
        list_q.push(a['Question']);
        list_t.push(120);
        list_i.push(a['ItemID']);
        list_v.push(a['VoicePath']);
        list_c.push(a['Category']);
        list_o.push(a['OptionList']);
        
        console.log(list_o);
        console.log(list_c);
        //console.log(list_q);
        //console.log(list_t);
        //console.log(list_i);
        //console.log(list_v);
        
        len_q = len_q + 1;
      }
    }
    
    xhr.send(json);

    var select=document.getElementById("area");
    var OptionAnswer = select.options[select.selectedIndex].value;
    console.log(OptionAnswer);
    document.getElementById('next').style='display:none';
    document.getElementById("ifrmid").contentWindow
            .postMessage(
              ('interviewid'+vid),
              background_rec
          );
    document.getElementById("ifrmid").contentWindow
            .postMessage(
            ('o'+OptionAnswer),
            background_rec
          );
    document.getElementById("ifrmid").contentWindow
            .postMessage(
            list_i[count],
            background_rec
          );
    document.getElementById("ifrmid").contentWindow
            .postMessage(
            2,
            background_rec
      );
    this.setState(state => ({
      seconds_think: 10
    }));
    document.getElementById("img1").src = './img/1000x337.gif';
    document.getElementById('change').textContent= '將於';
      document.getElementById('change1').style= 'display:inline';
    count = count + 1;
    if (count>=len_q){
      textend.innerHTML = '';
      document.getElementById("questions").textContent = '作答完畢' ;
      
      document.getElementById("next").hidden='true';
      clearInterval(this.interval);
      window.setInterval(("location="+"'"+end+"?aNO="+vid+"'"),1500);
    }else{
      this.setState(state => ({
        seconds: list_t[count]
        }));
      
      clearInterval(this.interval);
      document.getElementById("ifrmid").contentWindow
            .postMessage(
            3,
            background_rec
      );
      this.interval = setInterval(() =>this.Timer_think(), 1000);
      textend.innerHTML = '';
      document.getElementById("item").textContent= '第'+(count+1)+'題';
      document.getElementById("questions").textContent = list_q[count] ;
      console.log(list_c[count]);
      if(list_c[count] == "MCQ"){
        document.getElementById("item1").textContent= '請下拉選擇以下選項：';
        document.getElementById("myDIV").style.display = "block";
        document.getElementById("area").style.width = "300px";
        document.getElementById("area").style.height = "30px";
        var listo = [];
        len_o = list_o[count].length;
        for (var i=0;i<len_o;i++)
            {
              listo.push(list_o[count][i]['OptionContent']);
            }
        var id = new Array(1,2,3,4); 
        console.log(listo);
        var value = new Array(listo[0],listo[1],listo[2],listo[3]); 
        var select = document.getElementById("area"); 
        select.length = 1;
        select.options[0].selected = true;
        for(var x = 0;x<id.length;x++){ 
          var option = document.createElement("option"); 
          option.setAttribute("value",id[x]);
          option.appendChild(document.createTextNode(value[x])); 
          select.appendChild(option);
        } 
        this.setState(state => ({
          seconds_think: 2
        }));
      }else{
        document.getElementById("myDIV").style.display = "none";
      }
      document.getElementById('voice').src = list_v[count];
      document.getElementById("voice").autoplay;
      document.getElementById("voice").controls = false;
    }
    
  }

  Timer_think()
  {
    var textend = document.getElementById("speachend");
    //console.log(len_q);
    if (this.state.seconds_think==0){
      document.getElementById("img1").src = './img/1000x337.gif';
      this.state.seconds_think = this.state.seconds
      if(count==(len_q-1))
      {
        
        document.getElementById('next').style='display:inline';
      }else{

        document.getElementById('next').style='display:inline';
      }
      
      clearInterval(this.interval);
      this.interval = setInterval(() =>this.Timer_question(), 1000);
      textend.innerHTML = '';
    }
    else{
      this.setState(state => ({
        seconds_think: state.seconds_think - 1
        
      }));
    }
    
  }
  
  Timer_question()
  {
    
    if (this.state.seconds_think==0){
      document.getElementById("img1").src = './img/1000x337.gif';
      this.setState(state => ({
        seconds_think: 10
      }));
      document.getElementById('change').textContent= '將於';
      document.getElementById('change1').style= 'display:inline';
      //recognition.stop();
      this.nextquestion();

    }
    else{
      document.getElementById("img1").src = './img/02_1000x337.jpg';
      //recognition.start();
      document.getElementById('change').textContent= '作答剩餘秒數';
      document.getElementById('change1').style= 'display:none';
      this.setState(state => ({
        seconds_think: state.seconds_think - 1
    }));
    };
    
  }
  
  
  sendIt() {
    // 通过 postMessage 向子窗口发送数据
    document.getElementById("ifrmid").contentWindow
            .postMessage(
            1,
            background_rec
    );
  }

  render() {
    return (
      <div className='div_time'>
          <div className='timer_text'><span id="change" className='timer_text'>將於</span><span className='span_color'>{this.state.seconds_think}</span><span id="change1" className='timer_text'>開始面試</span></div>
          <div className='span_text'>
          
          <span >思考時間10秒，10秒後開始錄影（倒數計時120秒）</span>
          </div>
          <audio autoPlay id='voice' display='none'></audio>

        <p id='item' className='time_num'>第1題 </p><div className='text_size'>字體大小：<select id="mymenu"><option value="20">小</option><option value="30">中</option><option value="40">大</option></select></div>
        <div id="div_changesize"><p id='questions' className='time_quest'></p></div>
        <div id="myDIV"><span id='item1'></span><select name="area" id="area" className='select2'><option value="0">--請選擇--</option></select></div>
        <img src='./img/1000x337.gif' height='85%' width='100%' className='img' id="img1"></img>

        

        <button className='button2' id='next' hidden='True' onClick={this.nextquestion.bind(this)} >下一題</button>
        <Link to={{pathname:'/end'}}><button className='button2' id='end' hidden='True' onClick={this.nextquestion.bind(this)} >下一題</button></Link>
        <iframe src='/background' frameBorder='0' height='0' width='0' id='ifrmid' name='ifrmname'></iframe>
        <div>辨識結果：<span class="speech" id="speach"></span></div>
        <div>最終結果：<span class="speech" id="speachend"></span></div>
        
        
      </div>
      
    );
  }
}

export {Time}
