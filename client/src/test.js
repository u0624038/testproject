import React, { Component } from 'react'
import { render } from 'react-dom'
import { Redirect } from 'react-router'
import ScriptTag from 'react-script-tag';
import { HashRouter, Route, Link } from 'react-router-dom'
import {device_link} from './config'

class Test extends React.Component {
  constructor(props) {
    super(props);
    this.state = {interviewid:props.location.state.interviewid,audioid:props.location.state.audioid,videoid:props.location.state.videoid};
    console.log(this.state.audioid)
    console.log(this.state.videoid)
    //this.handle();
  }

  render(){
    return(
      
      <div className='div_device'>
      <div className='timer_text'>環境測試</div>
      
        <iframe src='/next' id='ifrmid' scrolling='no' frameBorder='0px' className='div_device'></iframe>
        
        <ScriptTag src="https://webrtc.github.io/adapter/adapter-latest.js"></ScriptTag>
        
        <ScriptTag src="./ga.js"></ScriptTag>
        <ScriptTag src="./main.js"></ScriptTag>
        <button id='myBtn' display='block' className='button7'>開始作答</button>
        <div id="myModal" className="modal" display='none'>
          <div className="modal-content">
          <text className='text2'>面試說明</text>
            <span className="close">&times;</span>
            <div >
              <img src='./img/Description.png' className="imaget"></img>
            </div>
            <button id='cancel' display='block' className='css_button'>取消</button><button id='confirm' display='block' className='css_button'>確認</button>
          </div>
        </div>
        
        <Link to={{pathname:'/time',state:{interviewid:this.state.interviewid,audioid:this.state.audioid,videoid:this.state.videoid}}}><button id='add_id' display='none' className='button7'>開始作答</button></Link>
      </div>
        
    );
  }
  
  componentDidMount(){

    let self = this
    var iFrame = document.getElementById('ifrmid');
    
    iFrame.onload = function(){
      //iframe加载完立即发送一条消息
      iFrame.contentWindow.postMessage( (self.state.videoid),
      device_link);

    }
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
    var modal = document.getElementById("myModal");
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
      modal.style.display = "block";
    }
    span.onclick = function() {
      modal.style.display = "none";
    }
    confirm.onclick = function() {
      document.getElementById("add_id").click();
    }
    cancel.onclick = function() {
      modal.style.display = "none";
    }
  }
}

export {Test}
