import React, { Component } from 'react'
import { render } from 'react-dom'
import { Redirect } from 'react-router'
import { HashRouter, Route, Link } from 'react-router-dom'
import './app.css'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value:''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkid =  this.checkid.bind(this);
  }

  componentDidMount() {
    document.getElementById('check_text').style='display:none';
    document.getElementById('add_id').style='display:none';
    var aipart = require('url');
    var ur = document.location.href;
    console.log(ur);
    var r = aipart.parse(ur).query;
    console.log(r);
    try{
      var splits = r.split('=');
      //console.log(splits[1]);
      document.getElementById('new-todo').value=splits[1];
    }catch(e){
      //console.log("no")
    }

  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }
  
  handleSubmit(e) {
    e.preventDefault();
  }
  
  checkid(){
    var aipart = require('url');
    var ur = document.location.href;
    console.log(ur);
    var r = aipart.parse(ur).query;
    console.log(r);
    try{
      var splits = r.split('=');
      console.log(splits[1]);
      if(splits[1]===''){
        var id = this.state.value;
      }else{
        document.getElementById('new-todo').value=splits[1];
        var id = splits[1];
      }
    }catch(e){
      //console.log("no")
      var id = this.state.value;
    }
    var data = {"InterviewID":id};
    var json = JSON.stringify(data);
    //console.log(json);
    
    var request = new XMLHttpRequest();
    request.open('POST', '/checkid');
    request.setRequestHeader('content-type' , 'application/json');
    request.onreadystatechange = function () {
      var modal = document.getElementById("myModal1");
      var span = document.getElementsByClassName("close1")[0];
      modal.style.display = "block";
      span.onclick = function() {
        modal.style.display = "none";
      }
      
        if (request.readyState == 4) {
          var result =JSON.parse(request.response);
          //console.log(result);
          if (request.response===JSON.stringify({InterviewID: 0})){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='無此面試代碼，請重新輸入';
          }
          else if(request.response===JSON.stringify({InterviewID: -1})){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='無此面試代碼，請重新輸入';
          }
          else if(request.response===JSON.stringify({InterviewID: 'no key'})){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='請輸入面試代碼';
          }
          else if(request.response===JSON.stringify({InterviewID: 2})){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='此面試代碼已使用';
          }
          else if(request.response===JSON.stringify({InterviewID: 1})){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='此面試代碼生成中';
          }
          else{
            document.getElementById('add_id').click();
          }
        }
        
    }
    
    request.send(json)
  }

  render() {
    return (
      
      <div className='div'>
        <div>
          <table width='795px'  className='t1'>
            <tr align="center" className='tr'>
              <td width='70%' className='td'>
              面試說明
              </td>
              <td width='30%' className='td'>
              請輸入面試代碼
              </td>
            </tr>
            <tr align="center">
              <td width='70%' className='td'>
                本場面試共計5題問答題，而面試官也會依據您的回答，最多再詢問2題延伸題。每題將於「10秒後」開始錄製作答，作答時間最長為「2分鐘」，回答完畢可提早進行下一題。面試過程無法中斷，請確認一切就緒後再開始。預祝您面試順利。
              </td>
              <td align="center">
                <input id="new-todo" type="text" onChange={this.handleChange} value={this.state.value} display='block' className='input'/>
              </td>
            </tr>
            <tr>
              <td colSpan='2'>
                <div >
                  <img src='./img/ai_i2a_01.jpg' height='87%' width='100%'></img>
                </div>
              </td>
            </tr>
            <tr>
              <td colSpan='2'>
                <div>
                  <button id='check' display='none' className="button1" onClick={this.checkid}>進入面試</button>
                  <Link to={{pathname:'/device',state:{interviewid:this.state.value}}}><button id='add_id' display='none' className="button1">進入面試</button></Link>
                </div>
                <div id="myModal1" className="modal1" display='none'>
                  <div className="modal-content1">
                    <span className="close1">&times;</span><br></br>
                    <div id='check_text' className="errtext">無此面試代碼，請重新輸入</div>
                  </div>
                </div>
                
              </td>
            </tr>
          </table>
        </div>
      </div>

    );
  }

  
}

export {App}
