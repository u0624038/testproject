import React, { Component } from 'react'
import { render } from 'react-dom'
import { Redirect } from 'react-router'
import ScriptTag from 'react-script-tag';
import { HashRouter, Route, Link } from 'react-router-dom'

class Device extends React.Component {
  constructor(props) {
    super(props);
    this.state = {interviewid:props.location.state.interviewid,audioid:0,videoid:0};
    
  }

  change(){
    var deid = document.getElementById('audioSource');
    console.log(deid)
  }

  componentDidMount(){    //问题延申：React数据获取为什么一定要在componentDidMount里面调用？
    let self = this; 
    
    document.getElementById('add_id').style='display:none';
    var x = 1   //为了避免作用域及缓存
    window.receiveMessageFromIndex = function ( event ) {
      document.getElementById('add_id').style='display:block';
        if(event!=undefined){
            //console.log(event.data );
            if (x == 1){
              self.setState({
                audioid:event.data  //2.给变量赋值
            })
            //console.log(self.state.audioid)
            x+=1;
            }
            else{
              self.setState({
                videoid:event.data  //2.给变量赋值
            })
            //console.log(self.state.videoid)
            }
        }
    }
    //监听message事件
    window.addEventListener("message", receiveMessageFromIndex, false);
};

  render(){
    return(
      <div className='div_device'>
      <iframe src='/deviceid' scrolling='no' frameBorder='0px' className='div_device1'　>
      </iframe>
        <Link to={{pathname:'/test',state:{interviewid:this.state.interviewid,audioid:this.state.audioid,videoid:this.state.videoid}}}><button id='add_id' display='block' className='button3'>環境測試</button></Link>
        
    </div>
        
    );
  }
  
}

export {Device}
