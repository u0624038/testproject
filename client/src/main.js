import React, { Component } from 'react'
import { render } from 'react-dom'
import { HashRouter, Route, Link } from 'react-router-dom'
import {Time} from './time'
import {App} from './app'
import ReactDOM from "react-dom"
import {Device} from './device'
import {Test} from './test'
import {End} from './end'


class Main extends React.Component {
  render() {
    if (navigator.userAgent.match(/Android/i)) {
      var isChrome = window.navigator.userAgent.indexOf("Chrome") !== -1;
      if (isChrome) {
        setTimeout(function() {
          if (!document.webkitHidden) {
            location.replace("https://play.google.com/store/apps/details?id=yochi.AIintverview");
          }
        }, 25);
        location.href='yochi://activity/interview';    
      } else {
        setTimeout(function() {
          if (!document.webkitHidden) {
            location.replace("https://play.google.com/store/apps/details?id=yochi.AIintverview");
          }
        }, 25);
        location.href='yochi://activity/interview';
      }
    } else if (navigator.userAgent.match(/(iPhone|iPad|iPod)/i)) {
      location.replace("https://apps.apple.com/tw/app/1111ai%E6%A8%A1%E6%93%AC%E9%9D%A2%E8%A9%A6%E7%B3%BB%E7%B5%B1/id1503456622");
    } else {
      return (
        <HashRouter>
            <div>
                {/*路徑指定/代表根目錄，所以預設就會渲染Home組件，
                而後方有/about的話會渲染About*/}
                <Route exact path="/" component={App} />
                <Route  path="/time" component={Time} />
                <Route  path="/device" component={Device} />
                <Route  path="/test" component={Test} />
                <Route  path="/end" component={End} />
            </div>
        </HashRouter>
      )
    }  
  }
}  

ReactDOM.render(
  <Main />
,document.getElementById('app'));