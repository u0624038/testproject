﻿import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import ejs from 'ejs'
import {sessionhost} from '../client/src/config'
import {answerhost} from '../client/src/config'
import {jobhost} from '../client/src/config'
import {uploadhost} from '../client/src/config'
import {checkhost} from '../client/src/config'
import {questionhost} from '../client/src/config'
import {followquestionhost} from '../client/src/config'


import fetch from 'fetch'
import { type } from 'os'
var request = require('request');
var FormData = require('form-data');
const http = require('http');
const https = require('https');
var fs = require('fs');
var uuidv1 = require('uuid/v1');
const multer = require('multer');
var privateKey  = fs.readFileSync('../ai1111ssl_put_here/private.key', 'utf8');
var certificate = fs.readFileSync('../ai1111ssl_put_here/ca.crt', 'utf8');
var ca_bundle = fs.readFileSync('../ai1111ssl_put_here/ca.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate , ca: ca_bundle};
var list_item=[];
var dgram = require('dgram');

let app = express();
const port = 3000;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
    cb(null, './uploads');  // 儲存的路徑，備註：需要自己建立
    },
    filename: function (req, file, cb) {
    // 將儲存檔名設定
    cb(null, uuidv1() + '.mkv'); 
    }
    }); 
  
var upload = multer({ storage: storage })
var id;
var len_q;
var count=0;
// view setup
app.set('views', path.join(__dirname, '../client/dist'));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);
app.use(logger('dev')); //命令行里面显示请求
app.use(bodyParser.json()); //解析json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser()); //解析cookie
app.use(express.static(path.join(__dirname, '../client/dist')));
app.get("/", (req, res) => {
    res.render("index")
})
app.get('/audiovideo', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/index.html'));
  });

app.get('/testrecord', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/test_record.html'));
    });

app.get('/background', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/background_record.html'));
}); 
app.get('/end', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/end.html'));
}); 

app.get('/next', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/next.html'));
});

app.get('/deviceid', (req, res) => {
    return res.sendFile(path.join(__dirname+'/templates/device.html'));
});
var FSessionid = [];
var data;
app.post('/audiovideo', upload.single('audiovideo'), (req, res) => {
    var file = req.file;
    var interviewid = req.headers['interviewid']
    var OptionAnswer = req.headers['optionanswer']
    var itemid = req.headers['itemid']
    console.log("B");
    console.log(FSessionid);
    console.log("C");
    console.log(file);
    console.log(interviewid)
    console.log(OptionAnswer)
    console.log(itemid)
    console.log('檔案名稱：%s', file.filename);
    console.log('檔案大小：%s', file.size);
    console.log('檔案路徑：%s', file.path);
    data = {"InterviewID":interviewid};
    var json = JSON.stringify(data);
    var Sessionid=[];
    request.post({
        url:sessionhost,
        headers:{'Content-Type': 'application/json'},
        body:json,
    }, function callback(error,response,body) {
        try{
            //console.log(JSON.parse(body));
            //console.log(JSON.parse(body)['SessionID'])
            Sessionid.push(JSON.parse(body)['SessionID']);
        }catch(error){
            
        }
        //console.log(Sessionid);
        data={'SessionID':FSessionid,"ItemID":itemid}
        json = JSON.stringify(data);
        console.log(json);
        var status=[];
        request.post({
            url:answerhost,
            headers:{'Content-Type': 'application/json'},
            body:json,
        }, function callback(error,response,body) {
            try{
                console.log(JSON.parse(body));
                status.push(JSON.parse(body)['Status'])
            }catch(error){

            }
            
            console.log(status)
        });
        var jobid=[];
        new Promise((resolve) => {
        
            request.post({
                url:jobhost,
                headers:{'Content-Type': 'application/json'},
                
            }, function callback(error,response,body) {
                try{
                    console.log(JSON.parse(body));
                    jobid.push(JSON.parse(body)['JobID']);
                }catch(error){

                }
                
                console.log(jobid);
                resolve(data ={"SessionID":FSessionid , "JobID":jobid , "DataSize":file.size});
                json=JSON.stringify(data);
                console.log(json);
                fs.rename(('./uploads/'+file.filename),('./uploads/'+(FSessionid+'.mkv')), function (err) {
                    if (err) throw err;
                    new Promise((resolve) => {
                        var Category;
                        if(OptionAnswer == "0"){
                            Category = "0";
                            OptionAnswer = Number(OptionAnswer);
                        }else{
                            Category = "MCQ";
                            OptionAnswer = Number(OptionAnswer);
                        }
                        console.log(OptionAnswer);
                        console.log("category："+Category);
                        var req = request.post(uploadhost, function (err, resp, body) {
                            if (err) {
                                console.log('Error!');
                            } else {
                                console.log(body);
                                //這邊寫log(body)

                                if (body == '{"Status":"ok"}'){
                                    var message = new Buffer(JSON.stringify({'InterviewID':interviewid,'os':'Web','ItemID':itemid,'SessionID':FSessionid,'status':'upload File OK'}))
                                    var client = dgram.createSocket('udp4')
                            
                                    client.send(message, 0, message.length, Port, host, function(err, bytes) {
                                        if (err) throw err;
                                        //console.log('udp message sent to ' + host + ':' + Port);
                                        client.close();
                                    });
                                }else{
                                    var message = new Buffer(JSON.stringify({'InterviewID':interviewid,'os':'Web','ItemID':itemid,'status':'upload File Error'}))
                                    var client = dgram.createSocket('udp4')
                            
                                    client.send(message, 0, message.length, Port, host, function(err, bytes) {
                                        if (err) throw err;
                                        //console.log('udp message sent to ' + host + ':' + Port);
                                        client.close();
                                    });
                                }

                                data ={"JobID":jobid}
                                json=JSON.stringify(data);
                                new Promise((resolve) => {
                                    request.post({
                                        url:checkhost,
                                        headers:{'Content-Type': 'application/json'},
                                        body:json
                                    }, function callback(error,response,body) {
                                        console.log(body);
                                        count+=1;
                                        console.log(count)
                                    });
                                        
                                }).catch(err => {
                                    console.log("error: " + err)
                                })
                            }
                        });
                        req.setHeader('jobid',jobid);
                        req.setHeader('sessionid',FSessionid);
                        req.setHeader('datasize',file.size);
                        req.setHeader('Category',Category);
                        req.setHeader('AnswerOption',OptionAnswer);
                        var form = req.form();
                        form.append('File', fs.createReadStream(('./uploads/'+FSessionid+'.mkv')), {
                        filename: FSessionid+'.mkv',
                        //contentType: 'text/plain'
                        });
                            
                            
                    }).catch(err => {
                        console.log("error: " + err)
                    })
                });           
            });
        }).catch(err => {
            console.log("error: " + err)
        });
        
    });
            
    return res.json({ message: "File Upload Sucess" });
});

app.post('/test', upload.single('File'), (req, res) => {
    var file = req.file;
    var header = req.headers.jobid;
    console.log(file);
    console.log(header);
    return res.json({ message: "File Upload Sucess" });
});

app.post('/request_question', (req, res) => {
    
    id = req.body.InterviewID;
    var data = {"InterviewID":id};
    var json = JSON.stringify(data);
    console.log(id);
    request.post({
        url:questionhost,
        headers:{'Content-Type': 'application/json'},
        body:json,
    }, function callback(error,response,body) {
        try{
            console.log(JSON.parse(body));
            len_q = JSON.parse(body)['QuestionList'].length;

            for (var i=0;i<len_q;i++)
            {
                list_item.push(JSON.parse(body)['QuestionList'][i]['ItemID']);
            }
            console.log(list_item);
            return res.send(body);
        }catch(error){

        }  
    });    
});


app.post('/checkid', (req, res) => {
    
    id = req.body.InterviewID;
    var data = {"InterviewID":id};
    var json = JSON.stringify(data);
    console.log(id);
    request.post({
        url:questionhost,
        headers:{'Content-Type': 'application/json'},
        body:json,
    }, function callback(error,response,body) {
        try{
            console.log(JSON.parse(body));
        
            if(body === JSON.stringify({InterviewID: 0})){

            }if(body === JSON.stringify({InterviewID: -1})){

            }if(body === JSON.stringify({InterviewID: 2})){

            }else{
                var message = new Buffer(JSON.stringify({'InterviewID':id,'status':'login'}))
                var client = dgram.createSocket('udp4')
        
                client.send(message, 0, message.length, Port, host, function(err, bytes) {
                    if (err) throw err;
                    console.log('udp message sent to ' + host + ':' + Port);
                    client.close();
                });
            }
            return res.send(body);
        }catch(error){

        }
          
    });
    
});

app.post('/FollowQuestion', (req, res) => {
    var itemID = req.body.ItemID;
    var words = req.body.Words;
    var datac = {"InterviewID":id};
    var jsons = JSON.stringify(datac);
    FSessionid=[];
    request.post({
        url:sessionhost,
        headers:{'Content-Type': 'application/json'},
        body:jsons,
    }, function callback(error,response,body) {
        try{
            console.log("D")
            console.log(JSON.parse(body));
            console.log("A");
            //console.log(JSON.parse(body)['SessionID'])
            FSessionid.push(JSON.parse(body)['SessionID']);
            console.log(words);
            var data = {"ItemID":String(itemID),"Words":String(words),"SessionID":String(FSessionid)};
            var json = JSON.stringify(data);
            console.log("5");
            console.log(json);
            console.log("6");
            console.log(id);
            request.post({
                url:followquestionhost,
                headers:{'Content-Type': 'application/json'},
                body:json,
            }, function callback(error,response,body) {
                
                
                return res.send(body);  
            });
        }catch(error){
            
        }
    });
});

function post_to_api(){
    var data = {"InterviewID":30};
    var json = JSON.stringify(data);
    request.post({
        url:questionhost,
        headers:{'Content-Type': 'application/json'},
        body:json,
    }, function callback(error,response,body) {
        try{
            console.log(JSON.parse(body));
        }catch(error){

        }
        
        //console.log(error);
        //list.append(body);
    }); 

    //console.log(list);
};

const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

/*httpServer.listen(8002, () => {
	console.log('HTTP Server running on port 8002');
});*/

httpsServer.listen(8001,'0.0.0.0', () => {
    console.log('HTTPS Server running on port 8001');
    
    //post_to_api();
});

//app.listen(port, () => {
//    console.log("server is running on port 3000");
//});
