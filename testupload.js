var express = require('express');
var https=require('https');
var app = express();
var cors = require('cors');
var fs = require("fs");
var bodyParser = require('body-parser');

var privateKey  = fs.readFileSync('../ai1111ssl_put_here/private.key', 'utf8');
var certificate = fs.readFileSync('../ai1111ssl_put_here/ca.crt', 'utf8');
var ca_bundle = fs.readFileSync('../ai1111ssl_put_here/ca.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate , ca: ca_bundle};

app.use(cors());
app.use('/public', express.static('public'));
app.use(bodyParser.json()); //解析json
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/', (req, res) => {   
   return res.send("success");
});

var httpsServer = https.createServer(credentials,app);
httpsServer.listen(8080,'0.0.0.0',()=>{
 
   var host = httpsServer.address().address
   var port = httpsServer.address().port
  
   console.log("訪問地址為 http://%s:%s", host, port)
  
});

